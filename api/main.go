package main

import (
	"github.com/gofiber/fiber/v2"
	routes "gitlab.com/Petrakan/blog/api/controllers"
)

func main() {

	//Инициализируем прриложение Fiber
	app := fiber.New()

	//Подключаем роуты
	routes.SetupRoutes(app)

	//Настройки порта
	app.Listen(":3000")
}
