package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/Petrakan/blog/api/controllers/handlers"
)

func SetupRoutes(app *fiber.App) {
	app.Get("/", handlers.GetHomepage)

	posts := app.Group("/posts")
	posts.Get("/", handlers.GetAllPosts)

}
