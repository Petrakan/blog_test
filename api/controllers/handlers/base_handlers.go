package handlers

import (
	"github.com/gofiber/fiber/v2"
)

func GetHomepage(c *fiber.Ctx) error {
	return c.SendString("This is Home page from Fiber api")
}
